/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robotproject1;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TableMap map = new TableMap(20,20);
        Robot robot = new Robot(2,2,'x',map,100);
        Bomb bomb = new Bomb(5,5);
        
        map.addObj(new Tree(10,10));
        map.addObj(new Tree(9,10));
        map.addObj(new Tree(10,9));
        map.addObj(new Tree(11,10));
        map.addObj(new Tree(5,10));
        map.addObj(new Tree(15,10));
        map.addObj(new Tree(15,11));
        map.addObj(new Tree(15,9));
        map.addObj(new Tree(15,14));
        map.addObj(new Tree(5,15));
        map.addObj(new Tree(15,11));
        map.addObj(new fuel(11,9,20));
        map.addObj(new fuel(15,12,20));
        map.addObj(new fuel(14,15,20));
        map.setBomb(bomb);
        map.setRobot(robot);
        
        
        while(true){
            map.showMap();
            char direction = inputDirection(sc);
            if(direction=='q'){
                printByeBye();
                break;
            }
            robot.walk(direction);
        }
        
    }

    private static void printByeBye() {
        System.out.println("ByeBye!!!");
    }

    private static char inputDirection(Scanner sc) {
        String str = sc.next();
        char direction = str.charAt(0);
        return direction;
    }
}

